#ifndef COLOR_H
#define COLOR_H

#include <SFML/Graphics.h>

void hlC_nextForegroundColor();
sfColor hlC_getBackgroundColor();
sfColor hlC_getForegroundColor();
char* hlC_getColorText();

#endif
