#ifndef DIE_H
#define DIE_H

#include <SFML/Graphics.h>
#include <stdlib.h>
#include "types.h"

typedef struct Die {
  int pos; // DiePosition
  int value;
  sfRectangleShape* square;
  sfCircleShape* eyes[6];
} Die;

int hlD_roll();
void hlD_draw(int pos);

#endif
