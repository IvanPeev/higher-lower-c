#ifndef GRAPHICS_STATIC_H
#define GRAPHICS_STATIC_H

#include <SFML/Graphics.h>
#include "color.h"
#include "settings.h"

sfRectangleShape* hlGS_createFieldOutline(sfColor fg, sfColor bg);
sfRectangleShape* hlGS_createScoreOutline(sfColor fg, sfColor bg);
sfRectangleShape* hlGS_createScoreVertLine(sfColor fg);
sfRectangleShape* hlGS_createScoreHorzLine(sfColor fg);
sfRectangleShape* hlGS_createBetOutline(sfColor fg, sfColor bg);
sfRectangleShape* hlGS_createMessageOutline(sfColor fg, sfColor bg);
sfText* hlGS_createTitle(sfColor fg, sfFont* font);
sfText* hlGS_createQuitText(sfColor fg, sfFont* font);
sfText* hlGS_createColorsText(sfColor fg, sfFont* font);
sfText* hlGS_createPotTitle(sfColor fg, sfFont* font);
sfText* hlGS_createStakeTitle(sfColor fg, sfFont* font);
sfText* hlGS_createBetTitle(sfColor fg, sfFont* font);
sfText* hlGS_createCPUTitle(sfColor fg, sfFont* font);
sfText* hlGS_createPlayerTitle(sfColor fg, sfFont* font);

#endif
