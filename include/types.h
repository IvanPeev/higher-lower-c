#ifndef TYPES_H
#define TYPES_H

typedef enum DiePosition {
  CPU_POS_1 = 35,
  CPU_POS_2 = 155,
  PLAYER_POS_1 = 435,
  PLAYER_POS_2 = 555
} DiePosition;

typedef enum GameStep {
  STEP_NEW,
  STEP_CPU,
  STEP_BET,
  STEP_ROUND_END,
  STEP_GAME_END
} GameStep;

typedef enum GameBet {
  BET_NONE,
  BET_HIGH,
  BET_LOW
} GameBet;

#endif
