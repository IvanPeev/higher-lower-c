#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "settings.h"
#include "types.h"
#include "variables.h"
#include "color.h"
#include "die.h"
#include "graphics-static.h"

sfText* message;
sfText* potText;
sfText* stakeText;
sfText* betText;
sfText* colorText;

GameStep currentStep = STEP_NEW;
GameBet bet = BET_NONE;
int pot = STARTING_POT;
int bid = 0;
int cpuDie1 = 0;
int cpuDie2 = 0;
int playerDie1 = 0;
int playerDie2 = 0;
// Die cpuDie1 = hlD_create(CPU_POS_1);
// Die cpuDie2 = hlD_create(CPU_POS_2);
// Die playerDie1 = hlD_create(PLAYER_POS_1);
// Die playerDie2 = hlD_create(PLAYER_POS_2);

void drawStaticGraphics(sfRenderWindow* window) {
  sfColor fg = hlC_getForegroundColor();
  sfColor bg = hlC_getBackgroundColor();

  sfRectangleShape* backgroundShape = sfRectangleShape_create();
  sfVector2f backgroundSize = { WINDOW_WIDTH, WINDOW_HEIGHT };
  sfRectangleShape_setSize(backgroundShape, backgroundSize);
  sfRectangleShape_setFillColor(backgroundShape, sfBlack);

  sfRectangleShape* fieldOutline = hlGS_createFieldOutline(fg, bg);
  sfRectangleShape* scoreOutline = hlGS_createScoreOutline(fg, bg);
  sfRectangleShape* scoreVertLine = hlGS_createScoreVertLine(fg);
  sfRectangleShape* scoreHorzLine = hlGS_createScoreHorzLine(fg);
  sfRectangleShape* betOutline = hlGS_createBetOutline(fg, bg);
  sfRectangleShape* messageOutline = hlGS_createMessageOutline(fg, bg);
  sfText* title = hlGS_createTitle(fg, hl_font);
  sfText* quitText = hlGS_createQuitText(fg, hl_font);
  sfText* colorsText = hlGS_createColorsText(fg, hl_font);
  sfText* potTitle = hlGS_createPotTitle(fg, hl_font);
  sfText* stakeTitle = hlGS_createStakeTitle(fg, hl_font);
  sfText* betTitle = hlGS_createBetTitle(fg, hl_font);
  sfText* cpuTitle = hlGS_createCPUTitle(fg, hl_font);
  sfText* playerTitle = hlGS_createPlayerTitle(fg, hl_font);

  sfRenderWindow_drawRectangleShape(window, backgroundShape, NULL);
  sfRenderWindow_drawRectangleShape(window, fieldOutline, NULL);
  sfRenderWindow_drawRectangleShape(window, scoreOutline, NULL);
  sfRenderWindow_drawRectangleShape(window, scoreVertLine, NULL);
  sfRenderWindow_drawRectangleShape(window, scoreHorzLine, NULL);
  sfRenderWindow_drawRectangleShape(window, betOutline, NULL);
  sfRenderWindow_drawRectangleShape(window, messageOutline, NULL);
  sfRenderWindow_drawText(window, title, NULL);
  sfRenderWindow_drawText(window, quitText, NULL);
  sfRenderWindow_drawText(window, colorsText, NULL);
  sfRenderWindow_drawText(window, potTitle, NULL);
  sfRenderWindow_drawText(window, stakeTitle, NULL);
  sfRenderWindow_drawText(window, betTitle, NULL);
  sfRenderWindow_drawText(window, cpuTitle, NULL);
  sfRenderWindow_drawText(window, playerTitle, NULL);

  sfRectangleShape_destroy(backgroundShape);
  sfRectangleShape_destroy(fieldOutline);
  sfRectangleShape_destroy(scoreOutline);
  sfRectangleShape_destroy(scoreVertLine);
  sfRectangleShape_destroy(scoreHorzLine);
  sfRectangleShape_destroy(betOutline);
  sfRectangleShape_destroy(messageOutline);
  sfText_destroy(title);
  sfText_destroy(quitText);
  sfText_destroy(colorsText);
  sfText_destroy(potTitle);
  sfText_destroy(stakeTitle);
  sfText_destroy(betTitle);
  sfText_destroy(cpuTitle);
  sfText_destroy(playerTitle);
}

void drawDynamicGraphics(sfRenderWindow* window) {
  colorText = sfText_create();
  sfVector2f colorTextPos = { 10, 7 };

  sfText_setFont(colorText, hl_font);
  sfText_setString(colorText, hlC_getColorText());
  sfText_setCharacterSize(colorText, 16);
  sfText_setFillColor(colorText, hlC_getForegroundColor());
  sfText_setPosition(colorText, colorTextPos);

  sfRenderWindow_drawText(window, colorText, NULL);

  sfText_destroy(colorText);
}

void handleWindowDrawing(sfRenderWindow* window) {
  drawStaticGraphics(window);
  drawDynamicGraphics(window);
}

void handleKeyEvent(sfRenderWindow* window, char key) {
  printf("unicode: %d\n", key);

  switch (key) {
    case sfKeyQ:
      sfRenderWindow_close(window);
      break;
    case sfKeyN:
      // TODO: write handler
      break;
    case sfKeyR:
      // TODO: write handler
      break;
    case sfKeyP:
      // TODO: write handler
      break;
    case sfKeyH:
      // TODO: write handler
      break;
    case sfKeyL:
      // TODO: write handler
      break;
    case sfKeyUp:
      // TODO: write handler
      break;
    case sfKeyDown:
      // TODO: write handler
      break;
    case sfKeyF:
      hlC_nextForegroundColor();
      break;
    default:
      break;
  }
}

void startWindow() {
  sfVideoMode mode = {WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_BITS_PER_PIXEL};
  sfRenderWindow* window = sfRenderWindow_create(mode, "Higher or lower in C", sfDefaultStyle, NULL);

  while (sfRenderWindow_isOpen(window)) {
    sfEvent event;

    while (sfRenderWindow_pollEvent(window, &event)) {
      if (event.type == sfEvtClosed) {
        sfRenderWindow_close(window);
      }
      
      if (event.type == sfEvtKeyPressed) {
        handleKeyEvent(window, event.text.unicode);
      }
    }

    sfRenderWindow_clear(window, sfTransparent);
    handleWindowDrawing(window);
    sfRenderWindow_display(window);
  }

  sfRenderWindow_destroy(window);
}

void loadFont() {
  hl_font = sfFont_createFromFile(FONT);
  
  if (!hl_font) {
    exit(EXIT_FAILURE);
  }
}

void startGame() {
  srand(time(NULL));

  loadFont();
  startWindow();
}
