#include <SFML/Graphics.h>
#define COLORS_LENGTH 7

int fgColorIdx = 0;

void hlC_nextForegroundColor() {
  fgColorIdx = (fgColorIdx + 1) % COLORS_LENGTH;
}

sfColor hlC_getBackgroundColor() {
  return sfBlack;
}

sfColor hlC_getForegroundColor() {
  switch (fgColorIdx) {
    case 0:
    default:
      return sfWhite;
    case 1:
      return sfRed;
    case 2:
      return sfGreen;
    case 3:
      return sfBlue;
    case 4:
      return sfYellow;
    case 5:
      return sfMagenta;
    case 6:
      return sfCyan;
  }
}

char* hlC_getColorText() {
  switch (fgColorIdx) {
    case 0:
    default:
      return "White";
    case 1:
      return "Red";
    case 2:
      return "Green";
    case 3:
      return "Blue";
    case 4:
      return "Yellow";
    case 5:
      return "Magenta";
    case 6:
      return "Cyan";
  }
}
