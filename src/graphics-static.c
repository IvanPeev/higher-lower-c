#include <SFML/Graphics.h>

#include "settings.h"

sfRectangleShape* hlGS_createFieldOutline(sfColor fg, sfColor bg) {
	sfRectangleShape* fieldOutline = sfRectangleShape_create();
	sfVector2f size = { DICE_OUTLINE_WIDTH, DICE_OUTLINE_HEIGHT };
	sfVector2f pos = { DICE_OUTLINE_X, DICE_OUTLINE_Y };

	sfRectangleShape_setFillColor(fieldOutline, bg);
	sfRectangleShape_setOutlineColor(fieldOutline, fg);
	sfRectangleShape_setOutlineThickness(fieldOutline, 5.f);
	sfRectangleShape_setSize(fieldOutline, size);
	sfRectangleShape_setPosition(fieldOutline, pos);

	return fieldOutline;
}

sfRectangleShape* hlGS_createScoreOutline(sfColor fg, sfColor bg) {
	sfRectangleShape* scoreOutline = sfRectangleShape_create();
	sfVector2f size = { SCORE_OUTLINE_WIDTH, SCORE_OUTLINE_HEIGHT };
	sfVector2f pos = { SCORE_OUTLINE_X, SCORE_OUTLINE_Y };

	sfRectangleShape_setFillColor(scoreOutline, bg);
	sfRectangleShape_setOutlineColor(scoreOutline, fg);
	sfRectangleShape_setOutlineThickness(scoreOutline, 3.f);
	sfRectangleShape_setSize(scoreOutline, size);
	sfRectangleShape_setPosition(scoreOutline, pos);

	return scoreOutline;
}

sfRectangleShape* hlGS_createScoreVertLine(sfColor fg) {
	sfRectangleShape* scoreVertLine = sfRectangleShape_create();
	sfVector2f size = { 3.f, 100.f };
	sfVector2f pos = { SCORE_OUTLINE_X + (SCORE_OUTLINE_WIDTH / 2), SCORE_OUTLINE_Y };

	sfRectangleShape_setFillColor(scoreVertLine, fg);
	sfRectangleShape_setSize(scoreVertLine, size);
	sfRectangleShape_setPosition(scoreVertLine, pos);

	return scoreVertLine;
}

sfRectangleShape* hlGS_createScoreHorzLine(sfColor fg) {
	sfRectangleShape* scoreHorzLine = sfRectangleShape_create();
	sfVector2f size = { 150.f, 3.f };
	sfVector2f pos = { SCORE_OUTLINE_X, SCORE_OUTLINE_Y + SCORE_OUTLINE_PADDING * 3 };

	sfRectangleShape_setFillColor(scoreHorzLine, fg);
	sfRectangleShape_setSize(scoreHorzLine, size);
	sfRectangleShape_setPosition(scoreHorzLine, pos);

	return scoreHorzLine;
}

sfRectangleShape* hlGS_createBetOutline(sfColor fg, sfColor bg) {
	sfRectangleShape* betOutline = sfRectangleShape_create();
	sfVector2f size =  { 200.f, 50.f };
	sfVector2f pos = { WINDOW_WIDTH / 4 - 50, WINDOW_HEIGHT - 80 };

	sfRectangleShape_setFillColor(betOutline, bg);
	sfRectangleShape_setOutlineColor(betOutline, fg);
	sfRectangleShape_setOutlineThickness(betOutline, 3.f);
	sfRectangleShape_setSize(betOutline, size);
	sfRectangleShape_setPosition(betOutline, pos);

	return betOutline;
}

sfRectangleShape* hlGS_createMessageOutline(sfColor fg, sfColor bg) {
	sfRectangleShape* messageOutline = sfRectangleShape_create();
	sfVector2f size = { 470.f, 100.f };
	sfVector2f pos = { WINDOW_WIDTH - 500, WINDOW_HEIGHT - 128 };

	sfRectangleShape_setFillColor(messageOutline, bg);
	sfRectangleShape_setOutlineColor(messageOutline, fg);
	sfRectangleShape_setOutlineThickness(messageOutline, 3.f);
	sfRectangleShape_setSize(messageOutline, size);
	sfRectangleShape_setPosition(messageOutline, pos);

	return messageOutline;
}

sfText* hlGS_createTitle(sfColor fg, sfFont* font) {
	sfText* title = sfText_create();
	sfVector2f pos = { WINDOW_WIDTH / 2 - 125, 100 };

	sfText_setFont(title, font);
	sfText_setString(title, "Higher or lower");
	sfText_setCharacterSize(title, 32);
	sfText_setFillColor(title, fg);
	sfText_setPosition(title, pos);

	return title;
}

sfText* hlGS_createQuitText(sfColor fg, sfFont* font) {
	sfText* quitText = sfText_create();
	sfVector2f pos = { DICE_OUTLINE_X, DICE_OUTLINE_Y - 30 };

	sfText_setFont(quitText, font);
	sfText_setString(quitText, "Press 'q' to exit");
	sfText_setCharacterSize(quitText, 16);
	sfText_setFillColor(quitText, fg);
	sfText_setPosition(quitText, pos);

	return quitText;
}

sfText* hlGS_createColorsText(sfColor fg, sfFont* font) {
	sfText* colorsText = sfText_create();
	sfVector2f pos = { DICE_OUTLINE_WIDTH - DICE_OUTLINE_X + 15, DICE_OUTLINE_Y - 30 };

	sfText_setFont(colorsText, font);
	sfText_setString(colorsText, "Toggle between colors with 'f'");
	sfText_setCharacterSize(colorsText, 16);
	sfText_setFillColor(colorsText, fg);
	sfText_setPosition(colorsText, pos);

	return colorsText;
}

sfText* hlGS_createPotTitle(sfColor fg, sfFont* font) {
	sfText* potTitle = sfText_create();
	sfVector2f pos = { SCORE_OUTLINE_X + SCORE_OUTLINE_PADDING * 1.25, SCORE_OUTLINE_Y + SCORE_OUTLINE_PADDING };

	sfText_setFont(potTitle, font);
	sfText_setString(potTitle, "POT");
	sfText_setCharacterSize(potTitle, 16);
	sfText_setFillColor(potTitle, fg);
	sfText_setPosition(potTitle, pos);

	return potTitle;
}

sfText* hlGS_createStakeTitle(sfColor fg, sfFont* font) {
	sfText* stakeTitle = sfText_create();
	sfVector2f pos = { SCORE_OUTLINE_WIDTH - SCORE_OUTLINE_PADDING * 2, SCORE_OUTLINE_Y + SCORE_OUTLINE_PADDING };

	sfText_setFont(stakeTitle, font);
	sfText_setString(stakeTitle, "STAKE");
	sfText_setCharacterSize(stakeTitle, 16);
	sfText_setFillColor(stakeTitle, fg);
	sfText_setPosition(stakeTitle, pos);

	return stakeTitle;
}

sfText* hlGS_createBetTitle(sfColor fg, sfFont* font) {
	sfText* betTitle = sfText_create();
	sfVector2f pos = { WINDOW_WIDTH / 5 + 10, WINDOW_HEIGHT - 65 };

	sfText_setFont(betTitle, font);
	sfText_setString(betTitle, "BET:");
	sfText_setCharacterSize(betTitle, 16);
	sfText_setFillColor(betTitle, fg);
	sfText_setPosition(betTitle, pos);

	return betTitle;
}

sfText* hlGS_createCPUTitle(sfColor fg, sfFont* font) {
	sfText* cpuTitle = sfText_create();
	sfVector2f pos = {
		DICE_OUTLINE_X + (DICE_OUTLINE_WIDTH / 4) - 50,
		DICE_OUTLINE_Y + DICE_OUTLINE_HEIGHT + 25
	};

	sfText_setFont(cpuTitle, font);
	sfText_setString(cpuTitle, "CPU");
	sfText_setCharacterSize(cpuTitle, 24);
	sfText_setFillColor(cpuTitle, fg);
	sfText_setPosition(cpuTitle, pos);

	return cpuTitle;
}

sfText* hlGS_createPlayerTitle(sfColor fg, sfFont* font) {
	sfText* playerTitle = sfText_create();
	sfVector2f pos = {
		DICE_OUTLINE_X + (DICE_OUTLINE_WIDTH / 2) + (DICE_OUTLINE_WIDTH / 4) - 50,
		DICE_OUTLINE_Y + DICE_OUTLINE_HEIGHT + 25
	};

	sfText_setFont(playerTitle, font);
	sfText_setString(playerTitle, "PLAYER");
	sfText_setCharacterSize(playerTitle, 24);
	sfText_setFillColor(playerTitle, fg);
	sfText_setPosition(playerTitle, pos);

	return playerTitle;
}
